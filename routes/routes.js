var express = require("express"),
  apiRouter = express();

apiRouter.use("/points", require("./points"));

module.exports = apiRouter;
