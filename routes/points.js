var express = require("express"),
  app = express();
var response = require("../api");

app.post("/points", function(req, res) {
  response.points(res, req.body.course_datetime, req.body.email);
});

module.exports = app;
