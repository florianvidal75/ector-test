const sqlite3 = require("sqlite3").verbose();
const Database = require("sqlite-async");
const moment = require("moment");

module.exports = {
  points: async function(res, course_dateTime, email) {
    async function getTotalRewards(email) {
      db_async = await Database.open("db/ector.db");
      var query =
        "SELECT SUM(rewardsvalue) FROM rewards WHERE email = " +
        `'` +
        email +
        `'`;
      const data = await db_async.get(query);
      return data;
    }

    let db = new sqlite3.Database(
      "db/ector.db",
      sqlite3.OPEN_READWRITE | sqlite3.OPEN_CREATE,
      err => {
        console.log(err);
      }
    );
    db.serialize(function() {
      db.run(
        "CREATE TABLE IF NOT EXISTS rewards (name TEXT, rewardsvalue INTEGER, email STRING);"
      );
    });
    // Start the process //
    const range00h = "00:00:00";
    const range9h = "09:00:00";
    const range20h = "20:00:00";
    var day_range_test = moment
      .utc(course_dateTime, "YYYY-MM-DD")
      .format("YYYY-MM-DD");
    let day_of_the_course = moment(course_dateTime).format("dddd");
    var body_response = null;
    var points_added = null;
    if (
      moment(course_dateTime).isBetween(
        day_range_test + "T" + range00h,
        day_range_test + "T" + range9h
      )
    ) {
      if (day_of_the_course === "Saturday" || day_of_the_course === "Sunday") {
        points_added = 20;
        db.run(
          `INSERT INTO rewards(rewardsvalue, email) VALUES(?, ?)`,
          [points_added, email],
          async function(err) {
            if (err) {
              return console.log(err);
            }
            data = await getTotalRewards(email);
            body_response = {
              total_rewards: data["SUM(rewardsvalue)"],
              email: email,
              points_added: points_added
            };
            res.json(body_response);
          }
        );
      } else {
        points_added = 10;
        db.run(
          `INSERT INTO rewards(rewardsvalue, email) VALUES(?, ?)`,
          [points_added, email],
          async function(err) {
            if (err) {
              return console.log(err);
            }
            data = await getTotalRewards(email);
            body_response = {
              total_rewards: data["SUM(rewardsvalue)"],
              email: email,
              points_added: points_added
            };
            res.json(body_response);
          }
        );
      }
    } else if (
      moment(course_dateTime).isBetween(
        day_range_test + "T" + range9h,
        day_range_test + "T" + range20h
      )
    ) {
      if (day_of_the_course === "Saturday" || day_of_the_course === "Sunday") {
        points_added = 10;
        db.run(
          `INSERT INTO rewards(rewardsvalue, email) VALUES(?, ?)`,
          [points_added, email],
          async function(err) {
            if (err) {
              return console.log(err);
            }
            data = await getTotalRewards(email);
            body_response = {
              total_rewards: data["SUM(rewardsvalue)"],
              email: email,
              points_added: points_added
            };
            res.json(body_response);
          }
        );
      } else {
        points_added = 5;
        db.run(
          `INSERT INTO rewards(rewardsvalue, email) VALUES(?, ?)`,
          [points_added, email],
          async function(err) {
            if (err) {
              return console.log(err);
            }
            data = await getTotalRewards(email);
            body_response = {
              total_rewards: data["SUM(rewardsvalue)"],
              email: email,
              points_added: points_added
            };
            res.json(body_response);
          }
        );
      }
    } else {
      if (day_of_the_course === "Saturday" || day_of_the_course === "Sunday") {
        points_added = 15;
        db.run(
          `INSERT INTO rewards(rewardsvalue, email) VALUES(?, ?)`,
          [points_added, email],
          async function(err) {
            if (err) {
              return console.log(err);
            }
            data = await getTotalRewards(email);
            body_response = {
              total_rewards: data["SUM(rewardsvalue)"],
              email: email,
              points_added: points_added
            };
            res.json(body_response);
          }
        );
      } else {
        points_added = 7;
        db.run(
          `INSERT INTO rewards(rewardsvalue, email) VALUES(?, ?)`,
          [points_added, email],
          async function(err) {
            if (err) {
              return console.log(err);
            }
            data = await getTotalRewards(email);
            body_response = {
              total_rewards: data["SUM(rewardsvalue)"],
              email: email,
              points_added: points_added
            };
            res.json(body_response);
          }
        );
      }
    }
    db.close();
  }
};
