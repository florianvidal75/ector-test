var bodyParser = require("body-parser");
var express = require("express"),
  app = express(),
  port = 3070;
var response = require("./api");
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

app.post("/points", function(req, res) {
  response.points(res, req.body.course_datetime, req.body.email);
});
app.listen(port, function(err) {
  console.log("running server on from port:" + port);
});
